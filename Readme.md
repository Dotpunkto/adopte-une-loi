# Install

## Clone project on your device
* git clone https://gitlab.com/Dotpunkto/adopte-une-loi.git
* cd **adopte-une-loi**

## Install the dependencies
* composer install

## Create and config .env.local
* Create **.env.local** file 
* Insert **"DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"** into your .env.local file and change **db_user, db_password and db_name** by your own settings.

## Create and load data in database
* php bin/console doctrine:database:create
* php bin/console doctrine:migrations:migrate
* php bin/console doctrine:fixtures:load

## Run NPM
* npm install
* npm run build

## Start server
* symfony server:start

# What's "Adopte une loi" ?

This project started with a group of 3 people as part of the Simplon formation. The goal was to create a website (an app) where people can propose laws, edit them, vote for them for a better futur.

The subject is simple : "Following the coronavirus crisis, we would like to offer you the development of a platform allowing citizens around the world to imagine a new system for the world. Our system is out of breath, the hyper optimization of it makes it fragile, what are the modifications to do to make it more resilient?

The main objective is to create a collaborative platform offering internet users a way to imagine, debate, unite, ... for the construction of a new model of society. A simple idea can bring big changes, become actors of your future!"

The deadline of this formation project is 4 weeks with a group of 3 persons : Theo, Lucas and Andrea. We devided each steps on a Trello and we worked hard to make it possible with learning a lot of new technologies, techniques and teamwork !

I hope this project touched your heart and we hope to see you again in the adventure of Adopte une loi.