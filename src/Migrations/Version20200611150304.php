<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611150304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, first_name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, img VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, lawid_id INT NOT NULL, userid_id INT NOT NULL, message LONGTEXT NOT NULL, createdat DATE NOT NULL, INDEX IDX_B6BD307F60E679FF (lawid_id), INDEX IDX_B6BD307F58E0A285 (userid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vote_article (id INT AUTO_INCREMENT NOT NULL, articleid_id INT NOT NULL, userid_id INT NOT NULL, vote_state TINYINT(1) DEFAULT NULL, interest TINYINT(1) DEFAULT NULL, INDEX IDX_898851FE9223694D (articleid_id), INDEX IDX_898851FE58E0A285 (userid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE law (id INT AUTO_INCREMENT NOT NULL, userid_id INT NOT NULL, name LONGTEXT NOT NULL, createdat DATETIME DEFAULT NULL, INDEX IDX_C0B552F58E0A285 (userid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, lawid_id INT NOT NULL, parentid_id INT DEFAULT NULL, userid_id INT NOT NULL, article LONGTEXT NOT NULL, state LONGTEXT DEFAULT NULL, createdat DATETIME DEFAULT NULL, amendment_time DATETIME DEFAULT NULL, INDEX IDX_23A0E6660E679FF (lawid_id), INDEX IDX_23A0E661F82D8F8 (parentid_id), INDEX IDX_23A0E6658E0A285 (userid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F60E679FF FOREIGN KEY (lawid_id) REFERENCES law (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F58E0A285 FOREIGN KEY (userid_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vote_article ADD CONSTRAINT FK_898851FE9223694D FOREIGN KEY (articleid_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE vote_article ADD CONSTRAINT FK_898851FE58E0A285 FOREIGN KEY (userid_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE law ADD CONSTRAINT FK_C0B552F58E0A285 FOREIGN KEY (userid_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6660E679FF FOREIGN KEY (lawid_id) REFERENCES law (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E661F82D8F8 FOREIGN KEY (parentid_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6658E0A285 FOREIGN KEY (userid_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F58E0A285');
        $this->addSql('ALTER TABLE vote_article DROP FOREIGN KEY FK_898851FE58E0A285');
        $this->addSql('ALTER TABLE law DROP FOREIGN KEY FK_C0B552F58E0A285');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6658E0A285');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F60E679FF');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6660E679FF');
        $this->addSql('ALTER TABLE vote_article DROP FOREIGN KEY FK_898851FE9223694D');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E661F82D8F8');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE vote_article');
        $this->addSql('DROP TABLE law');
        $this->addSql('DROP TABLE article');
    }
}
