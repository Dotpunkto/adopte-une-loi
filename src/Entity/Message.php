<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message = "not_blank")
     */
    private $message;

    /**
     * @ORM\Column(type="date")
     */
    private $createdat;

    /**
     * @ORM\ManyToOne(targetEntity=Law::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lawid;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function __construct()
    {
        $this->laws = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedat(): self
    {
        $this->createdat = new \DateTime();

        return $this;
    }

    public function getLawid(): ?Law
    {
        return $this->lawid;
    }

    public function setLawid(?Law $lawid): self
    {
        $this->lawid = $lawid;

        return $this;
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }
}
