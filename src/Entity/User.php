<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity=Law::class, inversedBy="users")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message = "invalid_mail")
     * @Assert\NotBlank(message = "not_blank_mail")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "not_blank")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "not_blank")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="invalid_password")
     * @Assert\NotBlank(message = "not_blank")
     */
    public $confirm_password;

    /**
     * @ORM\OneToMany(targetEntity=VoteArticle::class, mappedBy="userid")
     */
    private $voteArticles;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="userid")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity=Law::class, mappedBy="userid")
     */
    private $laws;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="userid")
     */
    private $messages;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(maxSize = "100k", mimeTypes={"image/jpeg"}, maxSizeMessage = "too_large", mimeTypesMessage = "wrong_type")
     */
    private $img;

    public function __construct()
    {
        $this->voteArticles = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->laws = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using bcrypt or argon
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|VoteArticle[]
     */
    public function getVoteArticles(): Collection
    {
        return $this->voteArticles;
    }

    public function addVoteArticle(VoteArticle $voteArticle): self
    {
        if (!$this->voteArticles->contains($voteArticle)) {
            $this->voteArticles[] = $voteArticle;
            $voteArticle->setUserid($this);
        }

        return $this;
    }

    public function removeVoteArticle(VoteArticle $voteArticle): self
    {
        if ($this->voteArticles->contains($voteArticle)) {
            $this->voteArticles->removeElement($voteArticle);
            // set the owning side to null (unless already changed)
            if ($voteArticle->getUserid() === $this) {
                $voteArticle->setUserid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setUserid($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getUserid() === $this) {
                $article->setUserid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Law[]
     */
    public function getLaws(): Collection
    {
        return $this->laws;
    }

    public function addLaw(Law $law): self
    {
        if (!$this->laws->contains($law)) {
            $this->laws[] = $law;
            $law->setUserid($this);
        }

        return $this;
    }

    public function removeLaw(Law $law): self
    {
        if ($this->laws->contains($law)) {
            $this->laws->removeElement($law);
            // set the owning side to null (unless already changed)
            if ($law->getUserid() === $this) {
                $law->setUserid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUserid($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUserid() === $this) {
                $message->setUserid(null);
            }
        }

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function __toString()
    {
        return $this->firstName;
    }
}
