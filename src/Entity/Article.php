<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use App\Entity\Law;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(min = 5, minMessage = "Your article is too short!")
     */
    private $article;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @ORM\OneToMany(targetEntity=VoteArticle::class, mappedBy="articleid")
     */
    private $voteArticles;

    /**
     * @ORM\ManyToOne(targetEntity=Law::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lawid;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="articles")
     */
    private $parentid;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="parentid")
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $amendmentTime;

    public function __construct()
    {
        $this->voteArticles = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function setArticle(string $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedat(): self
    {
        $this->createdat = new \DateTime();

        return $this;
    }

    /**
     * @return Collection|VoteArticle[]
     */
    public function getVoteArticles(): Collection
    {
        return $this->voteArticles;
    }

    public function addVoteArticle(VoteArticle $voteArticle): self
    {
        if (!$this->voteArticles->contains($voteArticle)) {
            $this->voteArticles[] = $voteArticle;
            $voteArticle->setArticleid($this);
        }

        return $this;
    }

    public function removeVoteArticle(VoteArticle $voteArticle): self
    {
        if ($this->voteArticles->contains($voteArticle)) {
            $this->voteArticles->removeElement($voteArticle);
            // set the owning side to null (unless already changed)
            if ($voteArticle->getArticleid() === $this) {
                $voteArticle->setArticleid(null);
            }
        }

        return $this;
    }

    public function getLawid(): ?Law
    {
        return $this->lawid;
    }
    
    public function setLawid(?Law $lawid): self
    {
        $this->lawid = $lawid;

        return $this;
    }

    public function getParentid(): ?self
    {
        return $this->parentid;
    }

    public function setParentid(?self $parentid): self
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(self $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setParentid($this);
        }

        return $this;
    }

    public function removeArticle(self $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getParentid() === $this) {
                $article->setParentid(null);
            }
        }

        return $this;
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getAmendmentTime(): ?\DateTimeInterface
    {
        return $this->amendmentTime;
    }

    public function setAmendmentTime(?\DateTimeInterface $amendmentTime): self
    {
        $this->amendmentTime = $amendmentTime;

        return $this;
    }

    public function endOfVotePhase(): ?\DateTime
    {
        $dateEnd = date_add($this->getCreatedat(), new \DateInterval('P0Y0DT1H0M'));
        if ($dateEnd < new \DateTime('now') && $this->getState() == null) {
            return $dateEnd;
        }
        return null;
    }

    public function endOfAmendmentPhase(): ?\DateTime
    {
        $dateEndVote = date_add(clone $this->getCreatedat(), new \DateInterval('P0Y0DT1H0M'));
        $dateEndAmendment = date_add(clone $this->getCreatedat(), new \DateInterval('P0Y0DT2H0M'));
        $now = new \DateTime('now');
        if ($now > $dateEndVote && $now < $dateEndAmendment && $this->getState() == null) {
            return $dateEndAmendment;
        }
        return null;
    }

    public function adopted()
    {
        return $this->getState() == 'adopted';
    }

    public function archived()
    {
        return $this->getState() == 'archived';
    }
}