<?php

namespace App\Entity;

use App\Repository\VoteArticleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoteArticleRepository::class)
 */
class VoteArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $voteState;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interest;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="voteArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articleid;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="voteArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoteState(): ?bool
    {
        return $this->voteState;
    }

    public function setVoteState(bool $voteState): self
    {
        $this->voteState = $voteState;

        return $this;
    }

    public function getInterest(): ?bool
    {
        return $this->interest;
    }

    public function setInterest(bool $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    public function getArticleid(): ?Article
    {
        return $this->articleid;
    }

    public function setArticleid(?Article $articleid): self
    {
        $this->articleid = $articleid;

        return $this;
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }
}
