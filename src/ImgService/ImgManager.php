<?php

namespace App\ImgService;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class ImgManager {

    private $slugger;

    public function __construct(SluggerInterface $slugger) {
        $this->slugger = $slugger;
    }

    public function getNameImg($imgFile, $directory): string {

        if ($imgFile) {
            $originalFilename = pathinfo($imgFile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$imgFile->guessExtension();
            try {
                $imgFile->move($directory, $newFilename);
            } catch (FileException $e) {
                dd($e);
            }
            return $newFilename;
        }

        return 'headshot_placeholder.jpg';
    }
}