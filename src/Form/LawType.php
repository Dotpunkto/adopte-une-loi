<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Law;
use App\Form\ArticleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class LawType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'text.name',
            ])
            ->add('articles', CollectionType::class, [
                'label' => false,
                'entry_type' => ArticleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => function (Article $article = null) {
                    return null === $article || empty($article->getArticle());
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Law::class,
        ]);
    }
}
