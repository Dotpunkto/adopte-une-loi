<?php

namespace App\Form;

use App\Entity\User;
use App\Filter\FilterLaw;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ArticleFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', ChoiceType::class, [
                'label' => 'filter.state', 
                'required' => false,
                'choices'  => [
                    'filter.vote' => 'vote',
                    'filter.archived' => 'archived',
                    'filter.adopted' => 'adopted',
                ],
            ])
            ->add('createdate', DateType::class, [
                'label' => 'filter.date',
                'widget' => 'single_text',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FilterLaw::class,
        ]);
    }
}
