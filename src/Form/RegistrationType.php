<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => true,
                'required' => true,
                'label' => 'register.email',
                ])
            ->add('firstName', TextType::class, [
                'label' => true,
                'required' => true,
                'label' => 'register.firstname',
                ])
            ->add('password', PasswordType::class, [
                'label' => true,
                'required' => true,
                'label' => 'register.password',
                ])
            ->add('confirm_password', PasswordType::class, [
                'label' => true,
                'required' => true,
                'label' => 'register.password_c'
            ])
            ->add('img' , FileType::class, [
                'data_class' => null,
                'required' => false,
                'label' => 'register.image',
                'help' => 'register.help',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
