<?php

namespace App\Form;

use App\Entity\User;
use App\Filter\FilterLaw;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LawFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false, 
                'required' => false,
                'attr' => [
                    'placeholder' => 'filter.name'
                ]
            ])
            ->add('createdate', DateType::class, [
                'label' => false, 
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'filter.date',
                    'class' => 'ml-3 mr-3'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FilterLaw::class,
        ]);
    }
}
