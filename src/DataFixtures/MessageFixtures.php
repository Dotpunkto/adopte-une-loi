<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Repository\LawRepository;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MessageFixtures extends AppFixtures implements DependentFixtureInterface
{
    private $userRepo;

    public function __construct(UserRepository $userRepo, LawRepository $lawRepo)
    {
        $this->userRepo = $userRepo;
        $this->lawRepo = $lawRepo;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Message::class, 50, function(Message $message, $count){
            $message->setMessage($this->faker->text)
                ->setLawid($this->lawRepo->find(rand(1, 10)))
                ->setUserid($this->userRepo->find(rand(1, 10)))
                ->setCreatedat($this->faker->dateTimeBetween('-100 days', '-1 days'));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LawFixtures::class,
            UserFixtures::class,
        );
    }
}