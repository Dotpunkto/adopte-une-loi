<?php

namespace App\DataFixtures;

use App\Entity\VoteArticle;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class VoteArticleFixtures extends AppFixtures implements DependentFixtureInterface
{
    private $articleRepo;
    private $userRepo;
    
    public function __construct(ArticleRepository $articleRepo, UserRepository $userRepo)
    {
        $this->articleRepo = $articleRepo;
        $this->userRepo = $userRepo;
    }
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(VoteArticle::class, 10, function(VoteArticle $article, $count){
            $article->setArticleid($this->articleRepo->find(rand(1, 10)))
                ->setUserid($this->userRepo->find(rand(1, 10)))
                ->setVoteState(false)
                ->setInterest(false);
        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ArticleFixture::class,
            UserFixtures::class,
        );
    }
}
