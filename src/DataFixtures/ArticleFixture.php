<?php

namespace App\DataFixtures;

use Faker\Generator;
use App\Entity\Article;
use Psr\Log\LoggerInterface;
use App\Repository\LawRepository;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;

class ArticleFixture extends Fixture implements DependentFixtureInterface
{
    private $lawRepo;
    private $userRepo;
    private $articleRepo;
    private $logger;
    /** @var Generator */
    protected $faker;

    public function __construct(LawRepository $lawRepo, UserRepository $userRepo, ArticleRepository $articleRepo ,LoggerInterface $logger)
    {
        $this->lawRepo = $lawRepo;
        $this->userRepo = $userRepo;
        $this->articleRepo = $articleRepo;
        $this->logger = $logger;
    }

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        for ($i=0; $i < 30; $i++){
            $article = new Article();
            $state = [
                'vote',
                'adopted',
                'archived'
            ];

            $article->setArticle($this->faker->text)
                ->setState($state[array_rand($state)])
                ->setCreatedat($this->faker->dateTimeBetween('-100 days', '-1 days'))
                ->setLawid($this->lawRepo->find(rand(1,10)))
                ->setUserid($this->userRepo->find(rand(1, 10)));

            if ($i > 0 and $this->faker->boolean(50)){
                $article->setParentid($this->articleRepo->find(rand(1, $i)));
            }

            $manager->persist($article);
            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            LawFixtures::class,
            UserFixtures::class,
        );
    }
}
