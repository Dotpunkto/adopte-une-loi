<?php

namespace App\DataFixtures;

use App\Entity\Law;
use App\Repository\UserRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LawFixtures extends AppFixtures implements DependentFixtureInterface
{
    private $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Law::class, 10, function(Law $law, $count){
            $law->setName($this->faker->name)
                ->setCreatedat($this->faker->dateTimeBetween('-100 days', '-1 days'))
                ->setUserid($this->userRepo->find(rand(1, 10)));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
