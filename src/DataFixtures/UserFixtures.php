<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends AppFixtures
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, 10, function(User $user, $count){
            $user->setFirstName(($this->faker->firstName))
                ->setEmail($this->faker->email)
                ->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    'engage'
                ))
                ->setImg('headshot_placeholder.jpg');
        });

        $user = new User();
        $user->setFirstName('Osef')
            ->setEmail('coucou@jesuisunmail.com')
            ->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'Admin'
            ))
            ->setRoles(['ROLE_ADMIN'])
            ->setImg('headshot_placeholder.jpg');
        $manager->persist($user);
        $manager->flush();
    }
}
