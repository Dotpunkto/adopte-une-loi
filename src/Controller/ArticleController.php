<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\AmendmentType;
use App\Repository\ArticleRepository;
use App\Repository\LawRepository;
use App\Repository\UserRepository;
use App\Repository\VoteArticleRepository;
use App\VoteSystem\VoteSystem;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ArticleController extends AbstractController
{
    /**
     * @Route("/amendments/law{lawId}/article{id}", name="amendments")
     */
    public function amendments(int $id, int $lawId, Article $article, VoteSystem $voteSystem, VoteArticleRepository $voteArticle, Security $security)
    {
        // dd($voteArticle->findLastAmendmentVoteOrNull($security->getUser(), $article));
        $amendments = $article->getArticles();

        return $this->render('article/amendments.html.twig', [
            'article' => $article,
            'amendments' => $amendments,
            'id' => $id,
            'lawId' => $lawId,
            'active' => "law",
        ]);
    }

    /**
     * @Route("/amendments/create/law{lawId}/article{id}", name="create_amendment")
     */
    public function createAmendment(int $id,int $lawId, LawRepository $lawRepository,UserRepository $userRepository, ArticleRepository $articleRepository, Request $request, Security $security)
    {
        $article = new Article();
        $form = $this->createForm(AmendmentType::class, $article);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $article = $form->getData();

            $article->setCreatedat(new DateTime('now'))
                ->setLawid($lawRepository->find($request->get('lawId')))
                ->setUserid($security->getUser())
                ->setParentid($articleRepository->find($id));

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('amendments', ['id' => $id, 'lawId' => $lawId]);
        }

        return $this->render('article/create_amendment.html.twig', [
            'form' => $form->createView(),
            'active' => "law",
        ]);
    }
}
