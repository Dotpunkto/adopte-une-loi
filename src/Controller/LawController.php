<?php

namespace App\Controller;

use App\Entity\Law;
use App\Form\LawType;
use App\Entity\Article;
use App\Entity\VoteArticle;
use App\Entity\Message;
use App\Filter\FilterLaw;
use App\Form\MessageType;
use App\Form\LawFilterFormType;
use App\FormService\FormService;
use App\Repository\LawRepository;
use App\Form\ArticleFilterFormType;
use App\Repository\ArticleRepository;
use App\Repository\VoteArticleRepository;
use App\VoteSystem\VoteSystem;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * @Route("/")
 */
class LawController extends AbstractController
{

    private $workflowRegistry;

    public function __construct(Registry $workflowRegistry)
    {
        $this->workflowRegistry = $workflowRegistry;
    }

    /**
     * @Route("/", name="law_index", methods={"GET","POST"})
     */
    public function index(Request $request, LawRepository $lawRepository): Response
    {
        $search = new FilterLaw();
        $form = $this->createForm(LawFilterFormType::class, $search);
        $form->handleRequest($request);
        $laws = $lawRepository->findSearch($search);
        return $this->render('law/index.html.twig', [
            'laws' => $laws,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/law/new", name="law_new", methods={"GET","POST"})
     */
    public function new(Request $request, FormService $formLaw, EntityManagerInterface $em): Response
    {
        $law = new Law();
        $user = $this->getUser();
        $form = $this->createForm(LawType::class, $law);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formLaw->lawForm($law, $user);
            $em->persist($law);
            $em->flush();

            return $this->redirectToRoute('law_index');
        }
        
        return $this->render('law/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/law/{id}", name="law_show", methods={"GET","POST"})
     */
    public function show(Request $request, Law $law, ArticleRepository $articleRepository, VoteSystem $voteSystem, EntityManagerInterface $em): Response
    {
        $articles = $law->getArticles();
        foreach ($articles as $article) {
            $voteSystem->transition($article);
        }
        // $endOfVotePhase = $articles[0]->endOfVotephase();

        // $endOfVotePhase = $voteSystem->calculEndOfVotePhase($law);

        // $voteSystem->transitionFromVoteToAmend($endOfVotePhase, $law);

        $user = $this->getUser();
        $search = new FilterLaw();
        $message = new Message();

        $form = $this->createForm(ArticleFilterFormType::class, $search);
        $form->handleRequest($request);
        $articles = $articleRepository->findSearch($search);

        $formComment = $this->createForm(MessageType::class, $message);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $message->setLawid($law);
            $message->setUserid($user);
            $em->persist($message);
            $em->flush();
        }

        return $this->render('law/show.html.twig', [
            'law' => $law,
            'articles' => $articles,
            'form' => $form->createView(),
            'formComment' => $formComment->createView(),
            'user' => $user,
            // 'endOfVotePhase' => $endOfVotePhase,
        ]);
    }

    /**
     * @Route("/law/{id}/edit", name="law_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Law $law, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(LawType::class, $law);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return $this->redirectToRoute('law_index');
        }

        return $this->render('law/edit.html.twig', [
            'law' => $law,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/law/{id}", name="law_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Law $law, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete'.$law->getId(), $request->request->get('_token'))) {
            $em->remove($law);
            $em->flush();
        }

        return $this->redirectToRoute('law_index');
    }

    /**
     * @Route("/{id}/api/{articleId}/{vote}", name="vote_api", requirements={"id":"\d+"})
     */
    public function vote(LoggerInterface $logger, VoteSystem $voteSystem, ArticleRepository $articleRepository, Security $security, VoteArticleRepository $voteArticleRepository, Request $request): JsonResponse
    {
        $this->denyAccessUnlessGranted('view', $this->getUser());
        $article = $articleRepository->find($request->get('articleId'));
        $voteSystem->transition($article);
        $user = $security->getUser();
        $voteIncoming = $request->get('vote') ? $request->get('vote') : null;

        if ($request->isMethod('POST')) {
            // $this->denyAccessUnlessGranted();
            $voteSystem->dynamicVote($article, $user, $voteIncoming);
        }

        $interestCount = count($voteArticleRepository->interests($article)) ? count($voteArticleRepository->interests($article)) : 0;
        $forCount = count($voteArticleRepository->fors($article)) ? count($voteArticleRepository->fors($article)) : 0;
        $againstCount = count($voteArticleRepository->againsts($article)) ? count($voteArticleRepository->againsts($article)) : 0;

        if ($voteIncoming == 'amendment') {
            $interestCount = count($voteArticleRepository->findBy(['articleid' => $article, 'interest' => true])) ? count($voteArticleRepository->findBy(['articleid' => $article, 'interest' => true])) : 0;
        }
        return $this->json([
            'interestCount' => $interestCount,
            'forCount' => $forCount,
            'againstCount' => $againstCount,
            ]);
    }


    /**
     * @Route("/comment/{id}", name="comment")
     */
    /*public function comment(Request $request, Law $law): Response {
        $message = new Message();
        $comment = $request->get('comment');
        $user = $this->security->getUser();
        if ($comment != "") {
            $entityManager = $this->getDoctrine()->getManager();
            $message->setLawid($law);
            $message->setUserid($user);
            $message->setMessage($comment);
            $entityManager->persist($message);
            $entityManager->flush();
        }
        return $this->redirectToRoute('law_show', ['id' => $law->getId()]);
    }
    */
}