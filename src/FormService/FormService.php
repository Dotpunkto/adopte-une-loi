<?php

namespace App\FormService;

use App\Entity\Law;
use App\Entity\User;
use App\ImgService\ImgManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class FormService {

    private $imgManager;
    private $encoder;

    public function __construct(ImgManager $imgManager, UserPasswordEncoderInterface $encoder) {
        $this->imgManager = $imgManager;
        $this->encoder = $encoder;
    }

    public function userForm($form, String $directory, User $user) {
        $imgFile = $form->get('img')->getData();
        $imgName = $this->imgManager->getNameImg($imgFile, $directory);
        $user->setImg($imgName);
        $hash = $this->encoder->encodePassword($user,$form->get('password')->getData());
        $user->setPassword($hash);
        $user->setEmail($form->get('email')->getData());
    }

    public function lawForm(Law $law, User $user) {
        $law->setUserid($user);
        $articles = $law->getArticles();
        foreach ($articles as $key => $article) {
            $article->setLawid($law);
            $article->setState('vote');
            $article->setUserId($user);
        }
    }
}