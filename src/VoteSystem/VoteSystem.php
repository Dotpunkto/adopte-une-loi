<?php

namespace App\VoteSystem;

use App\Entity\Law;
use App\Entity\User;
use App\Entity\Article;
use App\Entity\VoteArticle;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Workflow\Registry;
use App\Repository\VoteArticleRepository;

class VoteSystem
{
    private $articleRepository;
    private $workflowRegistry;
    private $em;
    private $voteArticleRepository;
    private $userRepository;

    public function __construct(ArticleRepository $articleRepository, Registry $workflowRegistry, EntityManagerInterface $em, VoteArticleRepository $voteArticleRepository, UserRepository $userRepository)
    {
        $this->articleRepository = $articleRepository;
        $this->workflowRegistry = $workflowRegistry;
        $this->em = $em;
        $this->voteArticleRepository = $voteArticleRepository;
        $this->userRepository = $userRepository;
    }

    public function calculEndOfVotePhase(?Law $law)
    {
        if ($articleOnvote = $this->articleRepository->findArticleFromLawWithVotestate($law)) {
            $endOfVotePhase = $articleOnvote ? $articleOnvote->getCreatedat() : null;
            $votePhaseDuration = new \DateInterval('P0Y0DT1H0M');
    
            date_add($endOfVotePhase, $votePhaseDuration);
    
            return $endOfVotePhase;
        }
        return null;
    }

    public function calculEndOfAmendmentPhase(Article $amendment)
    {
        $dateStart = $amendment->getAmendmentTime();
        $dateEnd = date_add($dateStart, new \DateInterval('P0Y0DT1H0M'));

        return $dateEnd;
    }

    public function twentyPercentUsers()
    {
        $nbOfUsers = count($this->userRepository->findAll());
        return ($nbOfUsers * 20 / 100);
    }

    // TODO : rajouter le test pour > contre pour pouvoir passer de vote à adopted
    public function transitionFromVoteToAmend(?Law $law): void
    {
        // $endOfVotePhase = $this->calculEndOfVotePhase($law);
        $articlesVoted = $this->articleRepository->findAllArticlesFromLawWithVotestate($law);
        $articleVoted = $articlesVoted[0];
        if ($articleVoted->endOfVotePhase()) {
            // $articlesWithVoteState = $this->articleRepository->findAllArticlesFromLawWithVotestate($law);

            foreach ($articlesVoted as $article) {
                $interest = count($this->voteArticleRepository->interests($article));
                $workflow = $this->workflowRegistry->get($article);

                if ($interest > $this->twentyPercentUsers()) {
                    $workflow->apply($article, 'amend');
                    $article->setAmendmentTime(new \DateTime('now'));
                } else {
                    $workflow->apply($article, 'archive');
                };
            }
            $this->em->flush();
        }
    }

    // tester
    public function transitionFromAmendToAdopteOrArchived(Article $article)
    {
        $amendments = $article->getArticles();
        $dateEnd = $this->calculEndOfAmendmentPhase($amendments[0]);
        $maxVotes = 0;
        $amendmentWithMaxVotes = null;

        if ($dateEnd < new \DateTime('now')) {

            foreach ($amendments as $amendment) {
                $amendmentVotes = count($this->voteArticleRepository->findBy(['articleid' => $amendment, 'interest' => true]));
                $maxVotes = $amendmentVotes > $maxVotes ? $amendmentVotes : $maxVotes;
                $amendmentWithMaxVotes = $amendment;
            }

            foreach ($amendments as $amendment) {
                $workflow = $this->workflowRegistry->get($amendment);
                $amendment == $amendmentWithMaxVotes ? $workflow->apply($amendment, 'adopte') : $workflow->apply($amendment, 'archive');
            }
            $this->em->flush();

        }
    }

    public function transition(Article $article)
    {
        if ($article->adopted() || $article->archived()) {
            return null;
        }

        if ($article->endOfAmendmentPhase() && $article->getArticles() == null) {
            return null;
        }

        if ($article->endOfVotePhase() && count($this->voteArticleRepository->interests($article)) <= $this->twentyPercentUsers()) {
            $article->setState('archived');
            // $this->em->persist($article);
            $this->em->flush();
        }

        if ($article->endOfVotePhase() && count($this->voteArticleRepository->fors($article)) > count($this->voteArticleRepository->againsts($article))) {
            $article->setState('adopted');
            $this->em->persist($article);
            $this->em->flush();
            return $article;
        }

        if ($article->endOfAmendmentPhase()) {
            $amendmentVoted = $this->articleRepository->amendmentWithMostVotes($article);
            $amendmentVoted->setState('adopted')
                ->setLawid($article->getLawid());
            $article->setState('archived');
            // $this->em->persist($amendmentVoted);
            // $this->em->persist($article);
            $this->em->flush(); // ??? obligé ?
            return $amendmentVoted;
        }

        return null;
    }

    public function dynamicVote(Article $article, User $user, string $vote)
    {
        $voteArticle = $this->voteArticleRepository->findOneBy(['userid' => $user, 'articleid' => $article]);
        $voteArticle ? $update = true : $update = false;

        if (!$update && $vote !== 'amendment') {
            $voteArticle = new VoteArticle();

            $voteArticle->setUserid($user)
                ->setArticleid($article);
        }

        switch ($vote) {
            case 'interest':
                $update ? $voteArticle->setInterest(!$voteArticle->getInterest()) : $voteArticle->setInterest(true);
                break;
            case 'for':
                $voteArticle->setVoteState(true);
                break;
            case 'against':
                $voteArticle->setVoteState(false);
                break;
            case 'amendment':
                $voteArticle = $this->amendmentVote($article, $user);
                break;
        }

        if (!$update && $vote !== 'amendment') {
            $this->em->persist($voteArticle);
        }
        $this->em->flush();
    }

    public function amendmentVote(Article $amendment, User $user)
    {
        $article = $this->articleRepository->find($amendment->getParentid());
        $amendments = $article->getArticles();

        foreach ($amendments as $oneAmendment) {
            if ($voteAmendment = $this->voteArticleRepository->findOneBy([
                'articleid' => $oneAmendment,
                'userid' => $user,
                'interest' => true
                ])) {
                $this->em->remove($voteAmendment);
            }
        }

        $voteAmendment = new VoteArticle();
        $voteAmendment->setArticleid($amendment)
            ->setUserid($user)
            ->setInterest(true);
        $this->em->persist($voteAmendment);

        return $voteAmendment;
    }
}