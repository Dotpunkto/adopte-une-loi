<?php

namespace App\Filter;

use App\Entity\User;
use DateTime;

class FilterLaw {

    /**
     * @var null\string
     */
    private $name = '';

    /**
     * @var null\date
     */
    private $createdate;

    /**
     * @var User[]
     */
    public $users = [];

    /**
     * @var nul\string
     */
    private $state;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedate(): ?\DateTime
    {
        return $this->createdate;
    }
    
    public function setCreatedate(?DateTime $dateTime): self
    {
        $this->createdate = $dateTime;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

}