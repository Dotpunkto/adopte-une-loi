<?php
namespace App\Security;

use App\Entity\Article;
use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ArticleVoter extends Voter
{
    const VOTE = 'vote';
    const AMEND = 'amend';

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::VOTE, self::AMEND])) {
            return false;
        }

        if (!$subject instanceof Article) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $article = $subject;
        
        switch ($attribute) {
            case self::VOTE:
                return $this->canVote($article) ? true : false;
            case self::AMEND:
                return $this->canAmend($article) ? true : false;
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canVote(Article $article)
    {
        return $article->endOfVotePhase() == null;
    }

    private function canAmend(Article $article)
    {
        if (!$this->canVote($article)) {
            return $article->endOfAmendmentPhase() == null;
        }
        return false;
    }
}