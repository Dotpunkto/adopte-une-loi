<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\VoteArticle;
use Doctrine\ORM\Query\Expr\Select;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method VoteArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method VoteArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method VoteArticle[]    findAll()
 * @method VoteArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VoteArticle::class);
    }

    public function interests($article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('v.interest = TRUE')
            ->getQuery()
            ->getResult()
        ;
    }

    public function fors($article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('v.voteState = TRUE')
            ->getQuery()
            ->getResult()
        ;
    }

    public function againsts($article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('v.voteState = FALSE')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getVoteInterestOrNull($user, $article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('userid = :user')
            ->setParameter('user', $user)
            ->andWhere('v.interest = TRUE')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getVoteForOrNull($user, $article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('userid = :user')
            ->setParameter('user', $user)
            ->andWhere('v.voteState = TRUE')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getVoteAgainstOrNull($user, $article)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.articleid = :article')
            ->setParameter('article', $article)
            ->andWhere('userid = :user')
            ->setParameter('user', $user)
            ->andWhere('v.voteState = FALSE')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findLastAmendmentVoteOrNull($user, $article)
    {
        return $this->createQueryBuilder('v')
            ->innerJoin('v.articleid', 'a')
            ->andWhere('a.parentid = :article')
            ->setParameter('article', $article)
            ->andWhere('v.userid = :user')
            ->setParameter('user', $user)
            ->andWhere('v.interest = TRUE')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // TODO TESTER
    public function amendmentWithMostVotes(Article $article)
    {
        return $this->createQueryBuilder('v')
            ->innerJoin('v.articleid', 'a')
            ->andWhere('a.parentid = :article')
            ->setParameter('article', $article)
            ->groupBy('v.articleid')
            ->andHaving('MAX(COUNT(v.interest))')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // /**
    //  * @return VoteArticle[] Returns an array of VoteArticle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VoteArticle
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
