<?php

namespace App\Repository;

use App\Entity\Law;
use App\Filter\FilterLaw;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Law|null find($id, $lockMode = null, $lockVersion = null)
 * @method Law|null findOneBy(array $criteria, array $orderBy = null)
 * @method Law[]    findAll()
 * @method Law[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LawRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Law::class);
    }

    public function findSearch(FilterLaw $search): array {
        $query = $this->createQueryBuilder('l');
            //->select('u', 'l')
            //->join('l.users', 'u');

        if (!empty($search->getName())) {
            $query = $query
                ->andWhere('l.name LIKE :name')
                ->setParameter('name', "%{$search->getName()}%");
        }

        if (!empty($search->getCreatedate())) {
            $date = $search->getCreatedate()->format('Y-m-d');
            $query = $query
                ->andWhere('l.createdat LIKE :createdate')
                ->setParameter('createdate', DATE($date));
                //dd($search);
        }
/*
        if (!empty($search->users)) {
            $query = $query
                ->andWhere('u.id IN (:users)')
                ->setParameter('users', $search->users);
        }*/
        //dd($query);
        return $query->getQuery()->getResult();
    }

    public function findAdopted()
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.articles', 'a')
            ->andWhere('a.state = :adopted')
            ->setParameter('adopted', 'adopted')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findArchived()
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.articles', 'a')
            ->andWhere('a.state = :archived')
            ->setParameter('archived', 'archived')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOnVotePhase()
    {
        $dateEnd = date_add(new \DateTime('now'), new \DateInterval('P0Y0DT1H0M'));

        return $this->createQueryBuilder('l')
            ->innerJoin('l.articles', 'a')
            ->andWhere('a.createdat < :dateEnd')
            ->setParameter('dateEnd', $dateEnd)
            ->getQuery()
            ->getResult()
        ;
    }

    // public function findOnAmendmentPhase()
    // {
    //     $amendmentStart = date_add(new \DateTime('now'), new \DateInterval('P0Y0DT1H0M'));
    //     $amendmentEnd = date_add(clone $amendmentStart, new \DateInterval('P0Y0DT1H0M'));

    //     return $this->createQueryBuilder('l')
    //         ->innerJoin('l.articles', 'a')
    //         ->andWhere('a.createdat > :')
    // }

    // /**
    //  * @return Law[] Returns an array of Law objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Law
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
