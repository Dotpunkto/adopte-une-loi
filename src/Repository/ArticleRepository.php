<?php

namespace App\Repository;

use App\Entity\Article;
use App\Filter\FilterLaw;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findAdoptedArticlesOfLaw($law)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.lawid = :law')
            ->setParameter('law', $law)
            ->andWhere('a.state = adopted')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findArchivedArticlesOfLaw($law)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.lawid = :law')
            ->setParameter('law', $law)
            ->andWhere('a.state = archived')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOnGoingArticlesOfLaw($law)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.lawid = :law')
            ->setParameter('law', $law)
            ->andWhere('a.state = :vote')
            ->setParameter('vote', 'vote')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findArticleFromLawWithVotestate($law)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.lawid = :law')
            ->setParameter('law', $law)
            ->andWhere('a.state = :vote')
            ->setParameter('vote', 'vote')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllArticlesFromLawWithVotestate($law)
    {
        $res = null;
        $onGoingArticles = $this->createQueryBuilder('a')
            ->andWhere('a.lawid = :law')
            ->setParameter('law', $law)
            ->andWhere('a.state = :null')
            ->setParameter('vote', null)
            ->getQuery()
            ->getResult()
        ;
        if ($onGoingArticles) {
            $res = array_reduce($onGoingArticles, function($carry, $item, $initial = []) {
                return $item->endOfVotePhase ? array_push($carry, $item) : $carry;
            });
        };
        return $res;
    }

    public function findSearch(FilterLaw $search): array {
        $query = $this->createQueryBuilder('a');
           // ->select('u', 'a')
           // ->join('a.users', 'u');

        if (!empty($search->getstate())) {
            $query = $query
                ->andWhere('a.state LIKE :state')
                ->setParameter('state', "%{$search->getstate()}%");
        }

        if (!empty($search->getCreatedate())) {
            $date = $search->getCreatedate()->format('Y-m-d');
            $query = $query
                ->andWhere('a.createdat LIKE :createdate')
                ->setParameter('createdate', DATE($date));
                //dd($search);
        }
/*
        if (!empty($search->users)) {
            $query = $query
                ->andWhere('u.id IN (:users)')
                ->setParameter('users', $search->users);
        }
*/
        return $query->getQuery()->getResult();
    }

    // TODO tester
    public function amendmentWithMostVotes($article)
    {
        return $this->createQueryBuilder('a')
        // return $this->em->create()
        //     ->select('a, COUNT(*)')
        //     ->from('article')
            ->andWhere('a.parentid = :article')
            ->setParameter('article', $article)
            ->innerJoin('a.voteArticles', 'v')
            ->groupBy('v.articleid')
            ->orderBy('COUNT(v.userid)', 'desc')
            // ->andHaving('MAX(COUNT(v.interest = :true))')
            // ->setParameter('true', true)
            // ->setMaxResults(1)
            ->getQuery()
            // ->getOneOrNullResult()
            ->getResult()
        ;
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
