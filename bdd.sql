﻿CREATE TABLE `User` (
    `id` int  NOT NULL ,
    `username` TEXT  NOT NULL ,
    `email` TEXT NOT NULL , 
    `roles` JSON NOT NULL ,
    `password` TEXT  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `Law` (
    `id` int  NOT NULL ,
    `name` TEXT NOT NULL ,
    `createdAt` Date  NOT NULL ,
    `userId` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `Article` (
    `id` int  NOT NULL ,
    `parentId` int NULL ,
    `article` TEXT NOT NULL ,
    `state` TEXT NOT NULL ,
    `createdAt` Date  NOT NULL ,
    `lawId` int  NOT NULL ,
    `userId` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `Vote_article` (
    `id` int  NOT NULL ,
    `articleId` int  NOT NULL ,
    `userId` int  NOT NULL ,
    `vote_state` boolean  NOT NULL ,
    `interest` boolean  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `Message` (
    `id` int  NOT NULL ,
    `message` TEXT NOT NULL ,
    `createdAt` Date  NOT NULL ,
    `LawId` int  NOT NULL ,
    `userId` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

ALTER TABLE `Law` ADD CONSTRAINT `fk_Law_userId` FOREIGN KEY(`userId`)
REFERENCES `User` (`id`);

ALTER TABLE `Article` ADD CONSTRAINT `fk_Article_parentId` FOREIGN KEY(`parentId`)
REFERENCES `Article` (`id`);

ALTER TABLE `Article` ADD CONSTRAINT `fk_Article_lawId` FOREIGN KEY(`lawId`)
REFERENCES `Law` (`id`);

ALTER TABLE `Article` ADD CONSTRAINT `fk_Article_userId` FOREIGN KEY(`userId`)
REFERENCES `User` (`id`);

ALTER TABLE `Vote_article` ADD CONSTRAINT `fk_Vote_article_articleId` FOREIGN KEY(`articleId`)
REFERENCES `Article` (`id`);

ALTER TABLE `Vote_article` ADD CONSTRAINT `fk_Vote_article_userId` FOREIGN KEY(`userId`)
REFERENCES `User` (`id`);

ALTER TABLE `Message` ADD CONSTRAINT `fk_Message_LawId` FOREIGN KEY(`LawId`)
REFERENCES `Law` (`id`);

ALTER TABLE `Message` ADD CONSTRAINT `fk_Message_userId` FOREIGN KEY(`userId`)
REFERENCES `User` (`id`);


INSERT INTO User VALUES(1, 'OsefUser', 12345);
INSERT INTO User VALUES(2, 'OsefUser1', 12345);
INSERT INTO User VALUES(3, 'OsefUser2', 12345);
INSERT INTO User VALUES(4, 'OsefUser3', 12345);
INSERT INTO User VALUES(5, 'OsefUser4', 12345);
INSERT INTO User VALUES(6, 'OsefUser5', 12345);

INSERT INTO Law VALUES(1, 'OsefLaw', CURDATE(), 1);
INSERT INTO Law VALUES(2, 'OsefLaw1', CURDATE(), 2);

INSERT INTO Article VALUES(1, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus massa purus, rhoncus ut rhoncus sed.', 'improved', CURDATE(), 1, 1);
INSERT INTO Article VALUES(2, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus massa purus, rhoncus ut rhoncus sed.', 'improved', CURDATE(), 2, 2);
INSERT INTO Article VALUES(3, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus', 'vote', CURDATE(), 2, 3);
INSERT INTO Article VALUES(4, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus massa', 'vote', CURDATE(), 1, 5);
INSERT INTO Article VALUES(5, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus massa purus,', 'vote', CURDATE(), 1, 4);
INSERT INTO Article VALUES(6, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus massa purus, rhoncus.', 'vote', CURDATE(), 2, 6);

INSERT INTO Message VALUES(1, 'Lorem ipsum dolor sit amet', CURDATE(), 1, 1);
INSERT INTO Message VALUES(2, 'Lorem ipsum dolor sit amet', CURDATE(), 1, 2);
INSERT INTO Message VALUES(3, 'Lorem ipsum dolor sit amet', CURDATE(), 2, 3);
INSERT INTO Message VALUES(4, 'Lorem ipsum dolor sit amet', CURDATE(), 2, 4);
INSERT INTO Message VALUES(5, 'Lorem ipsum dolor sit amet', CURDATE(), 2, 5);
INSERT INTO Message VALUES(6, 'Lorem ipsum dolor sit amet', CURDATE(), 1, 6);

INSERT INTO Vote_article VALUES(1, 1, 1, TRUE, TRUE);
INSERT INTO Vote_article VALUES(2, 2, 2, TRUE, TRUE);
INSERT INTO Vote_article VALUES(3, 2, 3, False, TRUE);
INSERT INTO Vote_article VALUES(4, 1, 4, False, TRUE);
INSERT INTO Vote_article VALUES(5, 1, 5, False, TRUE);
INSERT INTO Vote_article VALUES(6, 2, 6, False, TRUE);