document.addEventListener('DOMContentLoaded', vote);

function fetchInterest() {
    let links = document.getElementsByClassName('interest-btn');

    for (let i=0; i < links.length; i++) {
        fetch(links[i].href, {method: 'GET'}).then(function(response) {
            return response.json();
        }).then(function(data) {
            links[i].innerHTML = data.interestCount;
    })}
}

function vote() {
    let elements = document.getElementsByClassName('interest-btn');

    for (let i=0; i < elements.length; i++) {

        elements[i].onclick = (event => {
            event.preventDefault();

            fetch(elements[i].href, {method: 'POST'});
        })
    }

    fetchInterest();
}

setInterval(fetchInterest, 5000);