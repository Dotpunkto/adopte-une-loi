var $collectionHolder;
var $addArticleLink = $('<a href="#" class="add_article_link btn btn-outline-success"><p class="text-center" style="margin-top: 0px;margin-bottom: 0px;">Add a article</p></a>');
var $newLinkLi = $('<li style="list-style: none;"></li>').append($addArticleLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of articles
    var $collectionHolder = $('ul.articles');
    
    // add the "add a article" anchor and li to the articles ul
    $collectionHolder.append($newLinkLi);
    
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    
    $addArticleLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        
        // add a new article form (see code block below)
        addArticleForm($collectionHolder, $newLinkLi);
    });
    
});

function addArticleForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');
    
    // get the new index
    var index = $collectionHolder.data('index');
    
    // Replace '$$name$$' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);
    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    
    var $div = $('<div class="col"></div>').append(newForm);

    // Display the form in the page in an li, before the "Add a article" link li
    var $newFormLi = $('<li class="d-flex bd-highlight"></li>').append($div);
    // also add a remove button, just for this example
    $newFormLi.append('<a href="#" class="remove-article btn btn-outline-danger p-2 mt-5 ml-3 bd-highlight" style="height: 30px;width: 60px;"><p class="text-center" style="margin-top: -6px;">x</p></a>');
    $newLinkLi.before($newFormLi);
    // handle the removal, just for this example
    $('.remove-article').click(function(e) {
        e.preventDefault();
        
        $(this).parent().remove();
        
        return false;
    });
}