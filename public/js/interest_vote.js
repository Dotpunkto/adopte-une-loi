document.addEventListener('DOMContentLoaded', vote);

function fetchInterest() {
    let links = document.getElementsByClassName('interest-btn');
    let fors = document.getElementsByClassName('for');
    let againsts = document.getElementsByClassName('against');

    for (let i=0; i < links.length; i++) {
    fetch(links[i].href, {method: 'GET'}).then(function(response) {
        return response.json();
    }).then(function(data) {
        console.log(data);
        links[i].innerHTML = `interet ${data.interestCount}`;
        fors[i].innerHTML = `pour ${data.forCount}`;
        againsts[i].innerHTML = `contre ${data.againstCount}`;
    })}

}

function vote() {
    let elements = document.getElementsByClassName('interest-btn')
    let fors = document.getElementsByClassName('for');
    let againsts = document.getElementsByClassName('against');

    for (let i=0; i < elements.length; i++) {

        elements[i].onclick = (event => {
            event.preventDefault();

            fetch(elements[i].href, {method: 'POST'});
        })

        fors[i].onclick = (event => {
            event.preventDefault();

            fetch(fors[i].href, {method: 'POST'});
        })

        againsts[i].onclick = (event => {
            event.preventDefault();

            fetch(againsts[i].href, {method: 'POST'});
        })
    } 
}

setInterval(fetchInterest, 5000);