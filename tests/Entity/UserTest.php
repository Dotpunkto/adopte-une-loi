<?php
namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ConstraintViolation;

class UserTest extends KernelTestCase
{

    public function getEntity(): User
    {
        return (new User())
            ->setEmail('coucou@jesuisunmail.com')
            ->setFirstName('Osef')
            ->setPassword('12345');
    }

    public function assertHasErrors(User $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    // Invalid Test assert

    public function testInvalidBlankEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail(''), 1);
    }

    public function testInvalidEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('test'), 1);
    }

    public function testInvalidBlankFirstName()
    {
        $this->assertHasErrors($this->getEntity()->setFirstName(''), 1);
    }

    public function testInvalidBlankPassword()
    {   
        $this->assertHasErrors($this->getEntity()->setPassword(''), 1);
    }

    public function testInvalidImg()
    {
        $this->assertHasErrors($this->getEntity()->setImg('test'), 1);
    }

    // Valid Test assert

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testValidEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('coucou@jesuisunmail.com'), 0);
    }

    public function testValidFirstName()
    {
        $this->assertHasErrors($this->getEntity()->setFirstName('Osef'), 0);
    }

    public function testValidPassword()
    {   
        $this->assertHasErrors($this->getEntity()->setPassword('azerty'), 0);
    }

}