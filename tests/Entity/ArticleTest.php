<?php
namespace App\Tests\Entity;

use App\Entity\Article;
use App\Entity\Law;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class ArticleTest extends KernelTestCase
{

    public function getEntity(): Article
    {
        return (new Article())
            ->setArticle('test')
            ->setState('vote')
            ->setCreatedat(new \DateTime())
            ->setLawid(new Law())
            ->setParentid(NULL)
            ->setUserid(new User());
    }

    public function assertHasErrors(Article $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    // Invalid Test assert

    public function testInvalidBlankState()
    {
        $this->assertHasErrors($this->getEntity()->setState(''), 1);
    }

    public function testInvalidBlankArticle()
    {
        $this->assertHasErrors($this->getEntity()->setArticle(''), 1);
    }

    // Valid Test assert

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testValidBlankState()
    {
        $this->assertHasErrors($this->getEntity()->setState('vote'), 0);
    }

    public function testValidBlankArticle()
    {
        $this->assertHasErrors($this->getEntity()->setArticle('Article Test'), 0);
    }

}