<?php
namespace App\Tests\Entity;

use App\Entity\Message;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MessageTest extends KernelTestCase
{

    public function getEntity(): Message
    {
        return (new Message())
            ->setMessage('coucou');
    }

    public function assertHasErrors(Message $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    // Invalid Test assert

    public function testInvalidBlankMessage()
    {
        $this->assertHasErrors($this->getEntity()->setMessage(''), 1);
    }

    // Valid Test assert

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidMessage()
    {
        $this->assertHasErrors($this->getEntity()->setMessage('coucou'), 0);
    }
}